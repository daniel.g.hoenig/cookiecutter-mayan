from __future__ import absolute_import

from mayan.settings.base import *

SECRET_KEY = 'dummy-secret-key'

INSTALLED_APPS += (
    '{{ cookiecutter.app_name }}',
    'test_without_migrations',
)
