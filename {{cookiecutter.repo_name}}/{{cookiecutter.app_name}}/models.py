from __future__ import unicode_literals

# Import order
# 1- Python standard library
import logging

# 2- 3rd party Python libraries

# 3- Django standard apps
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

# 4- Django 3rd party apps

# 5- Mayan EDMS apps
from documents.models import DocumentType

# 6- Modules from this app
from .literals import DEFAULT_EXTRA_DATA
from .managers import DocumentExtraDataManager

# Global variables for this module go here, before class definitions
logger = logging.getLogger(__name__)


class DocumentExtraData(models.Model):
    """
    Model to store the additional data for each document.
    """

    document = models.OneToOneField(
        Document, related_name='extra_data',
        verbose_name=_('Document')
    )

    data = models.TextField(
        blank=True, default=DEFAULT_EXTRA_DATA,
        help_text=_('Extra data for this document.'),
        verbose_name=_('Template')
    )

    objects = DocumentExtraDataManager()
            
    # Model Meta class goes first, even before standard methods
    class Meta:
        verbose_name = _('Document extra data')
        verbose_name_plural = _('Documents extra data')

    # Django standard methods follow (__str__, delete, save)
    def __str__(self):
        return self.label
        
    # Finally, custom methods go here
