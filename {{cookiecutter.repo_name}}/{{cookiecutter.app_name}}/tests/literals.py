from __future__ import unicode_literals

# Contants user for tests go here.
import os

from documents.tests import TEST_SMALL_DOCUMENT_FILENAME

# If sample documents are needed for tests, use the ones provided by the 
# documents app or provide your own in the contrib folder.
TEST_SMALL_DOCUMENT_PATH = os.path.join(
    'contrib', 'sample_documents',
    TEST_SMALL_DOCUMENT_FILENAME
)
