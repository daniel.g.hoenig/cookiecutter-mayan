from __future__ import unicode_literals

from django.conf.urls import patterns, url

# API views reside in they own api_views module
from .api_views import APIDocumentExtraDataListView

from .views import ExtraDataListView, DocumentExtraDetailView

urlpatterns = patterns(
    '',
    # Do not specify the app's name in the url, it will be appended
    # automatically.
    url(
        r'^list/$', ExtraDataListView.as_view(),
        name='extra_data_list'
    ),
    # Add more URL context when adding views to existing apps or classes. 
    url(
        r'^document/(?P<pk>\d+)/extra_data/$',
        DocumentExtraDetailView.as_view(), name='document_extra_data_view'
    ),
)

# REST API URLs must be defined as a separate set of patterns
# Follow Django REST framework conventions when naming API views.
api_urls = patterns(
    '',
    url(
        r'^extra_data/$', APIDocumentExtraDataListView.as_view(),
        name='extra_data-list'
    ),
)
